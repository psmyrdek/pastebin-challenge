﻿using Newtonsoft.Json;
using Pastebin.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Mvc;

namespace Pastebin.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            var documents = ReadAllDocuments();

            return View(documents);
        }

        public ActionResult Show(string id)
        {
            var documents = ReadAllDocuments();

            Document document = documents.Where(x => x.Id == id).First();

            return View(document);
        }

        public ActionResult Add()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Add(Document document)
        {
            document.Id = Guid.NewGuid().ToString();
            System.IO.File.AppendAllText(String.Format("D://documents/{0}.txt", document.Id), JsonConvert.SerializeObject(document));
            
            return View("Summary", document);
        }

        private List<Document> ReadAllDocuments()
        {
            var documents = new List<Document>();

            foreach (string file in Directory.EnumerateFiles(@"D://documents/", "*.txt"))
            {
                string content = System.IO.File.ReadAllText(file);

                var document = JsonConvert.DeserializeObject<Document>(content);
                document.Language.ToString().ToLower();
                documents.Add(document);
            }

            return documents;
        }
    }
}