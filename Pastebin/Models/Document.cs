﻿using System.ComponentModel;
using System.Web.Mvc;

namespace Pastebin.Models
{
    public class Document
    {
        public string Id { get; set; }
        [AllowHtml]
        public string Body { get; set; }
        public Language Language { get; set; }
    }

    public enum Language
    {
        csharp,
        js
    }
}